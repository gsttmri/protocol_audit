# -*- coding: utf-8 -*-
"""
The purpose of this module is to provide a utility for creating a protocols table in a SQL database that would be
populated with using data from the Siemens protocol tree from the scanner.

The 'protocol' is defined by unique combination of 5 strings: scanner, region, exam, folder, sequence.
"""

import os
from sqlalchemy import create_engine, and_
from protocol_audit import Protocol
from sqlalchemy.orm import sessionmaker
from sqlalchemy.sql import exists
import datetime


def clean_sequence_string(dirty_string):
    """
    This is a helper method that removes excessive repetitions of '_clean' from filename that may have been
    inadvertedly created.
    :param dirty_string: String with ending with atleast one instance of '_clean'
    :return: String with '_clean' removed from end.
    """
    return dirty_string.replace('_clean', "")


def get_protocols(root_path):
    """
    Searches entire directory for file names that end with '_clean.txt'. For every such filename a tuple is returned
    that contains the all the folder in the path up to the root e.g. (scanner, region, exam, folder, filename).

    :param root_path: File path to root of protocol tree
    :return: List of tuples
    """
    protocols = []
    for root, dirs, files in os.walk(root_path):
        for item in files:
            if item.endswith('_clean.txt'):
                sequence_path = os.path.join(root, item)
                folder_path = os.path.dirname(sequence_path)
                exam_path = os.path.dirname(folder_path)
                region_path = os.path.dirname(exam_path)
                scanner_path = os.path.dirname(os.path.dirname(os.path.dirname(region_path)))

                scanner = os.path.basename(scanner_path)
                region = os.path.basename(region_path)
                exam = os.path.basename(exam_path)
                folder = os.path.basename(folder_path)
                sequence = clean_sequence_string(item.split('_clean.txt')[0])
                # get sequence name and remove excess _clean

                protocols.append((scanner, region, exam, folder, sequence))

    return protocols


def build(db_engine, current_working_directory):
    """
    This builds the protocols table
    :param: db_engine
    :param: current_working_directory
    :return:
    """
    Session = sessionmaker(bind=db_engine)
    session = Session()
    count = 0
    protocol_set = set(get_protocols(root_path=current_working_directory))
    protocols_added = []
    for scanner, region, exam, folder, sequence in protocol_set:
        new_protocol_record = Protocol(
            protocol_scanner=scanner, protocol_region=region, protocol_exam=exam, protocol_folder=folder,
            protocol_sequence=sequence)
        # print(sequence)

        if session.query(exists().where(and_(                   # check if new_protocol_record already exists
                Protocol.protocol_scanner == scanner,
                Protocol.protocol_region == region,
                Protocol.protocol_exam == exam,
                Protocol.protocol_folder == folder,
                Protocol.protocol_sequence == sequence
        ))).one()[0]:
            # print('Protocol exists')
            pass
        else:
            session.add(new_protocol_record)                    # if it doesn't then add
            protocols_added.append(str(new_protocol_record))
            count += 1
        print("\tAdded {}    ".format(count), end="\r")
    session.commit()
    session.close()
    log_file = os.path.join(current_working_directory, '{}_protocols.txt'.format(
        str(datetime.date.today()).replace("-", "")))

    with open(log_file, 'w') as f:
        f.write(str(protocols_added))

    print("\n")

if __name__ == '__main__':

    print('This program is designed to create a database table of unique protocols from a list of .edb files\n'
          'The current working directory is assumed to be the root of the tree that the program will traverse to find'
          'all .edb files.\n ')
    db_name = input('Enter the name of the database: ')
    home = os.getcwd()
    confirmed = input("The root directory is: {} \nIf this is not the correct directory, quit the program and change "
                      "to the correct directory or else type 'y'\n".format(home))

    if confirmed:
        start = datetime.datetime.now()
        engine = create_engine('postgresql://Haris@localhost/{}'.format(db_name, home))
        build(engine, home)
        print('Time elapsed ', datetime.datetime.now() - start)
        print('Finished building protocols table!')



