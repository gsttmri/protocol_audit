# -*- coding: utf-8 -*-
"""
Created on Mon Nov 30 17:12:31 2015

@author: Haris
"""

import os
import sys
import datetime
from protocol_audit import ProtocolParameterToday, Protocol, Parameter
from protocols_table import clean_sequence_string
from sqlalchemy.orm import sessionmaker
from sqlalchemy import create_engine, and_
from sqlalchemy.orm.exc import NoResultFound, MultipleResultsFound


def get_parameter_values(txt_file):
    """

    :param txt_file:
    :return:
    """

    parameter_values = {}
    # base = os.path.splitext(file_path)[0]
    # txt_file = base + '_clean.txt'  # This is done because the file path is of .edb file but we want the .txt file
    if not os.path.exists(txt_file):
        # print('{} does not exist'.format(txt_file))
        return False
    
    with open(txt_file) as f:
        # print("Opening file at " + file_path)
        for line in f:
            parameter = line.split()[0]
            value = line.split()[1]
            parameter_values[parameter] = value

    if not parameter_values:
        # print("_clean file is empty")
        return False
             
    return parameter_values

 
def get_protocol_id(session, file_path):
    # print(file_path)
    # sequence = os.path.basename(file_path).split('_clean')[0]      # removes .edb at the end
    sequence = clean_sequence_string(os.path.basename(file_path).split('_clean.txt')[0])
    folder_path = os.path.dirname(file_path)
    folder = os.path.basename(folder_path)
    exam_path = os.path.dirname(folder_path)
    exam = os.path.basename(exam_path)
    region_path = os.path.dirname(exam_path)
    region = os.path.basename(region_path)
    root_path = os.path.dirname(region_path)
    examdb_path = os.path.dirname(root_path)
    scanner_path = os.path.dirname(examdb_path)
    scanner = os.path.basename(scanner_path)

    print("Scanner : {3} Region: {0} Exam: {1} Folder: {2} Sequence: {4}".format(region, exam, folder, scanner, sequence))
    try:
        id_result = session.query(Protocol.id).filter(and_(Protocol.protocol_region == region,
                                                       Protocol.protocol_exam == exam,
                                                       Protocol.protocol_folder == folder,
                                                       Protocol.protocol_sequence == sequence,
                                                       Protocol.protocol_scanner == scanner)).one()
    except NoResultFound as e:
        print('Error: {}'.format(e))
        sys.exit(1)
    except MultipleResultsFound as e:
        print('Error: {}')
        sys.exit(1)

    return id_result

            
def get_parameter_id(session, parameter):
    id_result = session.query(Parameter.id).filter(Parameter.parameter_name == str(parameter)).one()
    return id_result


def get_sequence_paths(home):
    """

    :param home: Full path to the root directory of the protocol tree
    :return: List of file paths
    """
    sequence_list = []
    for root, dirs, files in os.walk(home):
        for item in files:
            if item.endswith("_clean.txt"):
                sequence_list.append(os.path.join(root, item))

    return sequence_list


def insert_into_database(session, file_path):
    # print("Inserting {} into database".format(file_path))
    protocol_id = get_protocol_id(session, file_path)
    # print("Protocol id is {}".format(protocol_id))
    parameter_values = get_parameter_values(file_path)
    time_date = datetime.datetime.now()
    today = str(datetime.date.today()).replace("-", "")
    for parameter, value in parameter_values.items():
        parameter_id = get_parameter_id(session, parameter)
        new_protocol_parameter_record = ProtocolParameterToday(protocol=protocol_id, parameter=parameter_id,
                                                               start_date=time_date, datum=value)
        session.add(new_protocol_parameter_record)
    session.commit()


def build(db_engine, current_working_directory):

    Session = sessionmaker(bind=db_engine)
    db_session = Session()

    seq_to_insert = get_sequence_paths(current_working_directory)
    print('Found {} sequences to insert into database'.format(len(seq_to_insert)))
    count = 0
    for sequence_path in seq_to_insert:
        insert_into_database(session=db_session, file_path=sequence_path)
        count += 1
        print('Sequences inserted: {}\n'.format(count))

    db_session.close()


if __name__ == "__main__":

    print('This program is designed to create a database table of unique protocols from a list of .edb files\n'
          'The current working directory is assumed to be the root of the tree that the program will traverse to find'
          'all .edb files.\n ')
    db_name = input('Enter the name of the database: ')
    home = os.getcwd()
    confirmed = input("The root directory is: {} \nIf this is not the correct directory, quit the program and change "
                      "to the correct directory or else type 'y'\n".format(home))

    if confirmed:
        start = datetime.datetime.now()
        engine = create_engine('postgresql://Haris@localhost/{}'.format(db_name))
        build(engine, home)
        print('Time elapsed ', datetime.datetime.now() - start)
        print('Finished building protocol_parameters table!')

