Welcome to the documentation for Protocol Audit!
================================================


Protocol Audit is a project initiated by the MR Physics group at Guy's & St. Thomas Hospitals in a bid to create a
semi-automated system of QA/QC of MRI protocols and sequence parameters.


Contents:

.. toctree::
   :maxdepth: 2

   modules


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

