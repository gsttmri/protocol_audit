# -*- coding: utf-8 -*-
"""
Created on Thu Dec  3 11:09:07 2015

@author: Haris
"""

import os
import dicom
import logging
from sqlalchemy import create_engine, and_, func
from sqlalchemy.orm import sessionmaker
from sqlalchemy.orm.exc import NoResultFound
from sqlalchemy.orm.exc import MultipleResultsFound
from protocol_audit import ProtocolParameter, Protocol, Translation, Parameter, Difference
import argparse
import datetime
import sys



special_cases = {'SliceThickness', 'RepetitionTime', 'EchoTime', 'FlipAngle', 'PercentSampling'}
# these dicom parameters have different formatting to the stored protocol parameters


def correct_special_case(parameter_name, incorrect_value):
    """
    Returns corrected DICOM parameter value to compate to stored protocol parameter value
    :param parameter_name: dicom parameter without spaces
    :param incorrect_value: string value to be changed
    :return: corrected string representation of value
    """

    if parameter_name == 'SliceThickness':
        return incorrect_value + '.0'
    if parameter_name == 'RepetitionTime':
        return incorrect_value + '000'
    if parameter_name == 'EchoTime':
        return incorrect_value + '000'
    if parameter_name == 'FlipAngle':
        return incorrect_value + '.0'
    if parameter_name == 'PercentSampling':
        return '0.' + incorrect_value


def potential_protocols(db_session, region, exam, sequence):

    protocol_query_result = db_session.query(
        Protocol.id, Protocol.protocol_folder).filter(
        and_(func.lower(Protocol.protocol_region) == func.lower(region),
             func.lower(Protocol.protocol_exam) == func.lower(exam),
             func.lower(Protocol.protocol_sequence) == func.lower(sequence))).all()

    # case insensitive search is achieved by forcing everything to lower case

    potential_result = {}  # dictionary where key is potential id and value is assoicated potential protocol_folder
    for record in protocol_query_result:
        potential_result[record[0]] = record[1]  # record ids, and protocol_folders

    return potential_result


def get_translated_protocol_parameters(translation_session):
    translated_parameters = translation_session.query(Translation.protocol_parameter).all()

    for index, parameter in enumerate(translated_parameters):
        translated_parameters[index] = translated_parameters[index][0].replace(" ", "")

    return translated_parameters


def get_dicom_parameters(translation_parameters, dicom_file):

    dicom_parameters = []
    for parameter in translation_parameters:
            dicom_parameters.append(dicom_file.data_element(parameter).value)

    return dicom_parameters


def guess_protocol(guess_session, potential_ids, dicom_file):
    """
    Return a guess of the correct protocol id.

    This function takes a list of potential protocol ids and tries to find the closest matching protocol by comparing
    the parameters in the dicom file with the possible stored protocols and choosing the most similar protocol.
    :param guess_session: Session connection to database
    :param potential_ids: Dictionary of potential ids {id:protocol_folder}
    :param dicom_file: Incoming dicom_file, pydicom object
    :return: Guessed protocol
    """

    similarity = 0
    most_similar = 0
    found_protocol_id = 0
    differences = {()}

    for protocol_id in potential_ids.keys():   # the keys of the dictionary are the potential Protocol.id values
        print(protocol_id)
        stored_parameters = guess_session.query(ProtocolParameter.parameter, ProtocolParameter.datum).filter(and_(
            ProtocolParameter.protocol == protocol_id, ProtocolParameter.end_date is None)).all()
        # get all parameters for this potential sequence, protocol
        print(stored_parameters)
        for stored_parameter in stored_parameters:
            print(stored_parameter)
            stored_parameter_id = stored_parameter[0]
            stored_parameter_value = stored_parameter[1].strip('"')

            stored_parameter_name = guess_session.query(Parameter.parameter_name).filter(
                Parameter.id == stored_parameter_id).one()
            # get the name of the protocol parameter as it is saved in .edb file

            try:
                translated_dicom_parameter = guess_session.query(Translation.sequence_parameter).filter(
                    Translation.protocol_parameter == stored_parameter_name).one()[0].replace(" ", "")
                # get the dicom equivalent name for parameter if it exists in translations table,
                # the [0].replace(" ",'') at the end is because query returns a tuple and the name has spaces
                # so this will get the first value of tuple which is the name and remove spaces.

            except NoResultFound as e:
                print('Warning: {}'.format(e))
                continue
            except MultipleResultsFound as e:
                print('Warning: {}'.format(e))
                continue

            if translated_dicom_parameter:
                print(translated_dicom_parameter)
                dicom_parameter_value = dicom_file.data_element(translated_dicom_parameter).value
                # get dicom value for the parameter
                try:
                    dicom_parameter_value = str(dicom_parameter_value)
                    if translated_dicom_parameter in special_cases:
                        dicom_parameter_value = correct_special_case(translated_dicom_parameter, dicom_parameter_value)
                        # some values are stored different in dicom i.e 150.0 versus 150, this is a helper function
                        # to correct some of those differences.
                except:
                    pass

                print('DICOM:{0}-{1}\nPROTOCOL:{2}-{3}'.format(dicom_parameter_value, type(dicom_parameter_value),
                                                                stored_parameter_value, type(stored_parameter_value)))

                if dicom_parameter_value == stored_parameter_value:
                    similarity += 1
                    print('SAME!')

                else:
                    print('DIFFERENT!')
                    differences.add((stored_parameter_id, dicom_parameter_value))

        if similarity > most_similar:
            log.info('Found a more similar protocol {}'.format(protocol_id))
            final_differences = differences
            found_protocol_id = protocol_id
            most_similar = similarity
            log.info(('Similarity degree = {}'.format(most_similar)))
            similarity = 0

    return found_protocol_id, final_differences


def date_time(dicom_f):
    time = dicom_f.AcquisitionTime.split('.')[0]
    time = "{0}{1}:{2}{3}".format(time[0], time[1], time[2], time[3])
    date = dicom_f.AcquisitionDate

    datetime = "{0} {1}".format(date, time)
    return datetime


def get_region_exam_sequence(d_file):

    region = d_file.StudyDescription.split('^')[0]
    log.info('The region of this dicom is {}'.format(region))
    exam = d_file.StudyDescription.split('^')[1]
    log.info('The exam of this dicom is {}'.format(exam))
    sequence = d_file.SeriesDescription
    log.info('The Series Description is {}'.format(sequence))

    return region, exam, sequence


def get_pid_differences(db_session, dicom_file):

    protocol_found = False
    region, exam, sequence = get_region_exam_sequence(dicom_file)

    dictionary_of_potential_protocols = potential_protocols(db_session, region, exam, sequence)

    if dictionary_of_potential_protocols is None:
        log.info("\n\t\t***SEQUENCE NAME COULD NOT BE FOUND - SKIPPING SEQUENCE****")
        return None, None

    differences = []

    number_of_potential_protocols = len(dictionary_of_potential_protocols)

    if number_of_potential_protocols > 1:

        log.info('Found multiple potential protocols, will attempt to find correct one')

        prot_id, differences = guess_protocol(db_session, dictionary_of_potential_protocols, dicom_file)
        if prot_id:
            protocol_found = True

    elif number_of_potential_protocols < 1:
        logging.warning(
            "NO POTENTIAL MATCHING PROTOCOLS FOUND!\n CHECK VALUES OF EXAM,REGION,SEQUENCE AND DB CONNECTION!\n")

    if protocol_found and len(differences) != 0:
        for difference in differences:
            if len(difference) != 2:
                continue
            print(difference)

            stored_protocol_data = db_session.query(
                ProtocolParameter.id,
                ProtocolParameter.datum).filter(
                and_(
                    ProtocolParameter.protocol == prot_id,
                    ProtocolParameter.parameter == difference[0],
                    ProtocolParameter.end_date is None)
            ).one()

            new_difference_record = Difference(protocol_parameter=stored_protocol_data[0],
                                               parameter=stored_protocol_data[1],
                                               parameter_datum=stored_protocol_data[2],
                                               dicom_datum=difference[1],
                                               dicom_datetime=date_time(dicom_file))
            db_session.add(new_difference_record)
        db_session.commit()

    log.info(prot_id)
    return prot_id, differences


def run_all():
    log.info('Running through current directory for all image files')
    home = os.getcwd()
    done_list = []
    skipped_list = []
    db = 'protocolaudit'
    user = 'protocolaudit'
    engine = create_engine('postgresql://{0}localhost/{1}'.format(db, user))
    Session = sessionmaker(bind=engine)
    db_session = Session()
    log.info('Connect to database {} with username {}'.format(db[0], db[1]))
    for root, dirs, files in os.walk(home):
        for f in files:
            if f.endswith('.dcm') and root not in done_list:
                log.info('Checking parameters of {}'.format(f))
                dicom_file = dicom.read_file(os.path.join(root,f))
                done = get_pid_differences(db_session, dicom_file)
                done_list.append(root)
                if done is None:
                    skipped_list.append(root)
    skipped = open(os.path.join(home, 'skipped.txt'))
    skipped.write(skipped_list.__str__())
    skipped.close()


if __name__ == '__main__':

    parser = argparse.ArgumentParser(description="logging")
    parser.add_argument("-d", "--debug", help="debug mode", action="store_const", dest="loglevel", const=logging.DEBUG,
                        default=logging.WARNING)
    parser.add_argument("-v", "--verbose", help="increase output verbosity", action="store_const", dest="loglevel",
                        const=logging.INFO)
    args = parser.parse_args()
    logging.basicConfig(level=args.loglevel)
    log = logging.getLogger('{}'.format(datetime.date.today()))
    ch = logging.StreamHandler(sys.stdout)
    log_file = os.path.join('DIFFERENCES_{0}.log'.format(datetime.date.today()))
    fh = logging.FileHandler(log_file)
    formatt = logging.Formatter('%(asctime)s %(name)-12s %(levelname)-8s %(message)s')
    ch.setLevel(logging.INFO)
    fh.setLevel(logging.DEBUG)
    fh.setFormatter(formatt)
    log.addHandler(ch)
    log.addHandler(fh)

    # db_name = input('Enter the name of the database: ')
    engine = create_engine('postgresql://Haris@localhost/{}'.format('protocol_audit'))
    Session = sessionmaker(bind=engine)
    session = Session()
    dicom_path = "/Users/Haris/Documents/STP/Specialism/Work/Protocol_and_Image_Audit/Example_Images/IM-0001-0001.dcm"
    dicom_file = dicom.read_file(dicom_path)
    protocol, differences = get_pid_differences(session, dicom_file)
    print('This sequence is from the {0} protocol and the differences are {1}'.format(protocol, differences))
