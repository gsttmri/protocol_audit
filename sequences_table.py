# -*- coding: utf-8 -*-
"""
The purpose of this module is to provide a utility for creating a sequences table in a SQL database that would be
populated with using data from the Siemens protocol tree from the scanner.

The 'sequence' is defined simply by the sequence name. The sequences table contains only unique sequence names but
does not differentiate between renaming of the same sequence without change of parameters.


.. todo::
    Convert this module to use SQLAlchemy

"""

import os
from sqlalchemy import create_engine
import sys
import logging
import argparse


def table(dbname, user, password, table_name, command):
    engine = create_engine('postgresql://{user}@localhost/{database}'.format(user=user, database=dbname))
    conn = engine.connect()

    if command:
        command_string = "CREATE TABLE %s (seq_id bigserial primary key, seq_name text);"
    if not command:
        command_string = "TRUNCATE TABLE %s;"

    try:
        conn.execute(command_string, (table_name,))
        conn.commit()

    except conn.DatabaseError as e:
        if conn:
            conn.rollback()
            print('Error %s' % e)
            sys.exit(1)

    if conn:
        conn.close()


def add_all_sequences(dbname, user, password, table_name, root_dir):
    logging.info(
        "This script will find all sequence files beneath the current working directory and then insert them into a "
        "database table named 'sequences'")

    home = root_dir

    logging.info("The home directory for the os.walk() is: {}".format(home))

    engine = create_engine('postgresql://Haris@localhost/{}'.format(dbname))
    conn = engine.connect()

    logging.info("The database name is {0} and the user is {1}".format(dbname, user))

    folders_list = []
    edb_list = []

    logging.info("Now commencing walk from home directory to find all .edb files and directories... ")

    for root, dirs, files in os.walk(home):
        for folder in dirs:
            folders_list.append(folder)
        for edb in files:
            if edb.endswith(".edb"):
                edb_list.append(edb)

    for edb in edb_list:
        if edb.strip('.')[0] in folders_list:
            edb_list.remove(edb)

    edb_set = set(edb_list)

    logging.info('The number of unique sequences found is:', len(edb_set))
    logging.debug('The length of the edb_list is %s and the length of the edb_set is %s', len(edb_list), len(edb_set))
    logging.debug('The list of sequences found in the os.walk from the home directory are : ', edb_set)

    for sequence in edb_list:
        if sequence.endswith(".edb"):
            sequence = os.path.splitext(sequence)[0]
            try:
                command_string = \
                    "INSERT INTO sequences (seq_name) SELECT seq_name FROM sequences " \
                    "UNION VALUES (%s) EXCEPT SELECT seq_name FROM sequences;"
                conn.execute(command_string, (sequence,))
                conn.commit()
                logging.info('Sequences left to add: {0}'.format(edb_list.pop()))

            except conn.DatabaseError as e:
                if conn:
                    conn.rollback()
                    print('Error %s' % e)
                    sys.exit(1)

    if conn:
        conn.close()

if __name__ == '__main__':

    parser = argparse.ArgumentParser(description="logging")
    parser.add_argument("-d", "--debug", help="debug mode", action="store_const", dest="loglevel", const=logging.DEBUG,
                        default=logging.WARNING)
    parser.add_argument("-v", "--verbose", help="increase output verbosity", action="store_const", dest="loglevel",
                        const=logging.INFO)
    args = parser.parse_args()
    logging.basicConfig(level=args.loglevel)