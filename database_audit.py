import os
import logging
from sqlalchemy import create_engine, and_, func, Table, ForeignKey, Column, Integer, String, DateTime, MetaData
from sqlalchemy import Sequence as sequence
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from protocol_audit import ProtocolParameter, Protocol, Parameter
import argparse
import datetime
import sys


def update_protocols(engine, home):
    """
    THIS NEEDS TO RETURN A LIST OF ADDED PROTOCOLS
    :param engine:
    :param home:
    :return:
    """
    import protocols_table
    protocols_table.build(engine, home)


def update_parameters(engine, db, home):
    import parameters_table
    parameters_table.build(engine, db, home)


def audit_today(engine, home):
    """
    This will check through the parameters to see if there have been any changes compared to the original database
    :return:
    """
    # process_edb(home)
    # update_protocols(engine, home)
    # update_parameters(engine, 'protocol_audit', home)
    import database_table
    database_table.build(engine, home)


def process_edb(home):
    """
    This method traverses the database Root and converts all .edb files into appropriate text files that can be read
    by other methods.
    :param home:
    :return: Success or error
    """
    count = 0
    for root, dirs, files in os.walk(home):
        for item in files:
            if item.endswith('.edb'):
                item_path = os.path.join(root, item)
                with open(item_path) as f:
                    is_protocol = False
                    parameters = {}
                    out_file = str(item).split('.edb')[0] + '_clean.txt'
                    out_path = os.path.join(root, out_file)
                    if os.path.isfile(out_file):
                        continue
                    for line in f:
                        if 'ASCCONV BEGIN' in line:
                            isProtocol = True
                            break
                    if is_protocol:
                        for line in f:
                            if 'ASCCONV END' in line:
                                break
                            parameter_name = line.replace(" ", "").split('=')[0]
                            value = line.replace(" ", "").split('=')[1]
                            parameters[parameter_name] = value

                        with open(out_path, 'w') as w:
                            for key, value in parameters.items():
                                record = key + " " + value
                                w.write(record)
                            count += 1
                        print("\tConverted {}    ".format(count), end="\r")
                    else:
                        log.debug('{} is not a protocol'.format(root, item))
    print("\n")

if __name__ == '__main__':

    parser = argparse.ArgumentParser(description="logging")
    parser.add_argument("-d", "--debug", help="debug mode", action="store_const", dest="loglevel", const=logging.DEBUG,
                        default=logging.WARNING)
    parser.add_argument("-v", "--verbose", help="increase output verbosity", action="store_const", dest="loglevel",
                        const=logging.INFO)
    args = parser.parse_args()
    logging.basicConfig(level=args.loglevel)
    log = logging.getLogger('{}'.format(datetime.date.today()))
    ch = logging.StreamHandler(sys.stdout)
    log_file = os.path.join('DIFFERENCES_{0}.log'.format(datetime.date.today()))
    fh = logging.FileHandler(log_file)
    formatt = logging.Formatter('%(asctime)s %(name)-12s %(levelname)-8s %(message)s')
    ch.setLevel(logging.INFO)
    fh.setLevel(logging.DEBUG)
    fh.setFormatter(formatt)
    log.addHandler(ch)
    log.addHandler(fh)

    Base = declarative_base()

    database = 'protocol_audit'
    engine = create_engine('postgresql://Haris@localhost/{}'.format(database))
    Session = sessionmaker(bind=engine)
    session = Session()

