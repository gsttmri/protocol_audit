import unittest
import image_audit


class ImageAuditTestCase(unittest.TestCase):
    def setUp(self):
        self.parameter_name = 'SliceThickness'
        self.incorrect_value = '5'

    def test_correct_special_cases(self):
        self.assertEqual(image_audit.correct_special_case(self.parameter_name, self.incorrect_value), '5.0',
                         'special case not corrected')

if __name__ == '__main__':
    unittest.main()