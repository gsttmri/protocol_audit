"""
This module defines the tables (and their relations) that make up the Protocol Audit Database.

Each table is defined as a Python class that inherits from the base class from SQLAlchemy. SQLAlchemy is the Python SQL
toolkit and Object Relational Mapper that gives application developers the full power and flexibility of SQL.
It provides a full suite of well known enterprise-level persistence patterns, designed for efficient and high-performing
database access, adapted into a simple and Pythonic domain language.

There is a single function declared in this module (build_tables) that only needs to be called once in the lifetime of
the database in order to create the tables in the database. Once created, the tables persist.
"""

from sqlalchemy import ForeignKey, Column, Integer, String, DateTime
from sqlalchemy import Sequence as sequence
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship
from sqlalchemy import create_engine
import datetime

# import protocols_table
# import parameters_table
# import database_table
# import os

Base = declarative_base()
date = str(datetime.date.today()).replace("-", "")


class Translation(Base):
    """
    The Translation class defines the 'translations' table. Each instance of the Translation class relates to a record
    in the 'translations' table. A record in the 'translations' table contains: id, sequence_parameter, protocol_parameter.
    """
    __tablename__ = 'translations'

    id = Column(Integer, sequence('translations_id_seq'), primary_key=True)
    """
    Unique identifier of record
    """
    sequence_parameter = Column(String, nullable=False)
    """
    String of parameter name from the DICOM header
    """
    protocol_parameter = Column(String, nullable=False)
    """
    String of parameter name from the .edx files from the protocol tree.
    """

    def __repr__(self):
        return "<User(sequence_parameter='%s', protocol_parameter='%s')>" % (
            self.sequence_parameter, self.protocol_parameter)


class Parameter(Base):
    __tablename__ = 'parameters'
    id = Column(Integer, sequence('parameter_id_seq'), primary_key=True)
    parameter_name = Column(String, nullable=False)

    # database_table = relationship("ProtocolParameter", back_populates="parameters_table")

    def __repr__(self):
        return "<Parameter(parameter_name='%s')>" % self.parameter_name


class Protocol(Base):
    __tablename__ = 'protocols'
    id = Column(Integer, sequence('protocols_id_seq'), primary_key=True)
    protocol_scanner = Column(String, nullable=False)
    protocol_region = Column(String, nullable=False)
    protocol_exam = Column(String, nullable=False)
    protocol_folder = Column(String, nullable=False)
    protocol_sequence = Column(String, nullable=False)

    # database_table = relationship("ProtocolParameter", back_populates="protocols_table")

    def __repr__(self):
        return "<Protocol(protocol_scanner={0}, protocol_region={1}, protocol_exam={2}, protocol_folder={3}, " \
               "protocol_sequence={4})>".format(
            self.protocol_scanner, self.protocol_region, self.protocol_exam, self.protocol_folder,
            self.protocol_sequence)


class ProtocolParameter(Base):
    __tablename__ = 'protocol_parameters'
    id = Column(Integer, sequence('protocol_parameters_id_seq'), primary_key=True)
    protocol = Column(Integer, ForeignKey('protocols.id'), primary_key=True)
    parameter = Column(Integer, ForeignKey('parameters.id'), primary_key=True)
    # protocol = Column(Integer)
    # parameter = Column(Integer)
    start_date = Column(DateTime, primary_key=True)
    end_date = Column(DateTime)
    datum = Column(String)

    # parameters_table = relationship("Parameter", back_populates="database_table")
    # protocols_table = relationship("Protocol", back_populates="database_table")

    def __repr__(self):
        return '<ProtocolParameter(id={0}, protocol={1}, parameter={2}, start_date={3}, end_date={4}, datum={5} >'. \
            format(self.id, self.protocol, self.parameter, self.start_date, self.end_date, self.datum)


class ProtocolParameterToday(Base):

    __tablename__ = 'protocol_parameters_{}'.format(str(datetime.date.today()).replace('-', ""))
    id = Column(Integer, sequence('protocol_parameters_id_seq'), primary_key=True)
    protocol = Column(Integer, ForeignKey('protocols.id'), primary_key=True)
    parameter = Column(Integer, ForeignKey('parameters.id'), primary_key=True)
    start_date = Column(DateTime)
    end_date = Column(DateTime)
    datum = Column(String)

    def __repr__(self):
        return '<ProtocolParameterToday(id={0}, protocol={1}, parameter={2}, datum={3} >'. \
            format(self.id, self.protocol, self.parameter, self.datum)


class Difference(Base):
    __tablename__ = 'differences'
    id = Column(Integer, sequence('differences_id_seq'), primary_key=True)
    protocol_parameter = Column(Integer, ForeignKey('protocol_parameters.id'), primary_key=True)
    parameter_datum = Column(String, nullable=False)
    dicom_datum = Column(String, nullable=False)
    dicom_datetime = Column(DateTime, nullable=False)

    difference_table = relationship("ProtocolParameter")

    def __repr__(self):
        return '<Difference(protocol={0}, parameter={1}, parameter_datum={2}, dicom_datum={3}, dicom_datetime={4} >'. \
            format(self.protocol, self.parameter, self.parameter_datum, self.dicom_datum, self.dicom_datetime)


def build_tables(db_name):
    engine = create_engine('postgresql://Haris@localhost/{}'.format(db_name))
    Base.metadata.create_all(engine)

# def build_and_fill(db_name='template1'):
#     build_tables(db_name)
#     engine = create_engine('postgresql://Haris@localhost/{}'.format(db_name))
#     home = os.getcwd()
#     parameters_table.build(engine, home)
#     protocols_table.build(engine, home)
#     database_table.build(engine, home)
