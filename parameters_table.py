# -*- coding: utf-8 -*-
"""
Created on Mon Nov 30 10:16:38 2015

@author: Haris
"""
import os
import sys
from sqlalchemy import create_engine
from protocol_audit import Parameter
from sqlalchemy.orm import sessionmaker
import datetime


def build(engine, db, home_dir):
    """
    This function adds unique parameter names from a file tree of text files to the parameter table.

    The current working directory is assumed to be the root of the tree that contains the parameter .txt files for each
    sequence. The text files end in '_clean.txt' as they have been converted from csv files and been formatted. If the
    parameters table does not exist then it will be created according the schema defined in protocol_audit.Parameter .

    The file tree is traversed and each '_clean.txt' file is opened and the first word of each line is stored in a list
    as a potential parameter to be added to the table. Once all the parameters in a file have been read the file is
    closed. The parameter list is then iterated over and the Parameter table is queried using session.query to see
    if it returns any rows - if no rows are return then the parameter is added to the session and session is committed.

    :param engine: The sqlalchemy engine is the interface for connecting to the database
    :param db: The name of the database {String}
    :param home_dir: The directory to be traversed
    :return: None
    """

    Session = sessionmaker(bind=engine)
    session = Session()                                     # create session for sending SQL to database

    if engine.dialect.has_table(session, 'parameters'):    # check if database contains parameters table
        print('Parameters table exists')
    else:
        print('Parameters table does not exist!\n')
        create = input('Do you want to have a parameters table created in this database?')  # ask user if they want it

        if create:
            try:
                import protocol_audit                       # import definition of all tables in database
                protocol_audit.build_tables(db)             # create all tables that don't exist

            except:
                session.rollback()
                print('Could not create parameter table!')
                sys.exit(0)

        else:
            print('Without a parameters table the build function cannot run')
            sys.exit(0)
    parameters_added = []
    for root, dirs, files in os.walk(home_dir):

        for filename in files:

            if filename.endswith("_clean.txt"):             # parameter files derived from .edb files

                file_path = os.path.join(root, filename)
                params_in_csv = []

                with open(file_path) as f:
                    # print("Opening file at ", file_path)

                    for line in f:
                        params_in_csv.append(line.split()[0])   # record first word of every line

                for parameter_name in params_in_csv:

                    parameter = Parameter(parameter_name=parameter_name)   # create new parameter record

                    if not session.query(Parameter.parameter_name).filter(Parameter.parameter_name == parameter_name).all():
                        # if new parameter does not already exist
                        #print('Adding {}')
                        session.add(parameter)
                        parameters_added.append(params_in_csv)

                    else:
                        # print('{} already exists!'.format(parameter_name))
                        pass
    session.commit()
    session.close()
    log_file = os.path.join(home_dir, '{}_parameters.txt'.format(str(datetime.date.today()).replace("-", "")))

    with open(log_file, 'w') as f:
        f.write(str(parameters_added))

if __name__ == '__main__':

    print('This program is designed to create a database table of unique parameters from a list of text files created'
          'from .edb files.\n'
          'The current working directory is assumed to be the root of the tree that the program will traverse to find'
          'all *_clean.txt files.\n ')

    db_name = input('Enter the name of the database: ')
    home = os.getcwd()
    confirmed = input("The root directory is: {} \nIf this is not the correct directory, quit the program and change "
                      "to the correct directory or else type 'y'\n".format(home))

    if confirmed:
        start = datetime.datetime.now()
        db_engine = create_engine('postgresql://Haris@localhost/{}'.format(db_name))
        build(engine=db_engine, db=db_name, home_dir=home)
        print('Time elapsed ', datetime.datetime.now() - start)
        print('Finished building parameters table!')
    else:
        sys.exit(0)
